========================================
Organizational change: Mediation Program
========================================

- Date proposed: 2021-12-07
- RFC MR: https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/9

Summary
-------

Add an official mediation program to our organization consisting of elected
mediators to help settle disputes and provide a clear escalation path.

Motivation
----------

Currently, we lack an established way to settle personal disputes. Furthermore,
we do not provide a visible escalation path, making resolving a dispute or
acquiring help for a two-or-more-sided resolution intransparent. Without
mediators and an escalation path, we risk people emotionally resigning when a
dispute cannot be resolved independently. The mediation program helps to
mitigate the risk of losing contributors and aims to prevent social barriers
between people. Our staff and volunteers are our most valuable resource after
all.

Specification
-------------

General expectations
++++++++++++++++++++

* Do not avoid conflicts, but try to resolve them sensibly.
* Step in when you see questionable behavior and explain what you do not
  like about it to those involved.
* Articulate your problem with someone else's behavior directly, or through
  someone you trust. That way, a person gets the opportunity to share their
  view or change potentially problematic behaviour.

Point of contact
++++++++++++++++

In the event of a problem, you can usually turn to one of the staff available
who will try to help you with your problem or support you in interacting with
the other person.

You are, of course, also welcome to contact the elected mediators or the project
leader to arrange a mediation.

Mediators
+++++++++

- Mediators are elected by all staff for a period of two years.

- The group of mediators consists of three members.

- The tasks of the mediators include:
    - Be the point of contact for staff members and external persons in the
      event of disputes or irritations
    - Liaise between staff members or between staff members and external
      persons to clarify and help identify concrete issues
    - Strive to help by clear communication and acceptance

- The elected mediators are part of the clarification body.

- The project leader may not be elected as a mediator. In case a mediator gets
  elected as project leader, the position as mediator needs to be vacated.

- Mediators are required to be active for their position to hold any ground.
  As such, inactive mediators are removed by the project leader after a period
  of two weeks of inactivity when there is a demand of their mediator duties
  related to their responsibilities. A month of plain inactivity or absence
  would be a sign of ignoring their duties and would trigger removal.

Clarification Body
++++++++++++++++++

- The clarification body consists of the mediators and, on a case by case basis,
  the project leader.

- If required, each affected person is allowed to declare one confidant who
  additionally participates in the mediation. Each confidant is not expected to
  take over the clarification on behalf of the persons but instead to assist
  them in case they do not feel comfortable on their own.

- The task of the clarification body is to clarify disputes amongst the staff
  members or between staff members and external persons. The goal of the
  clarification body should be an amicable settlement of the disputing parties.
  The rules of procedure of the clarification body, which the clarification
  body gives itself, regulate the details.

- If possible, dispute resolution with the affected parties should, by default,
  take place with voice or video communication to aid a fluent conversation and
  provide a personal layer that helps to deescalate. If required, the clarification
  body should moderate the conversation to give all participants the appropriate
  room to respond or express themselves.

Escalation path
+++++++++++++++

- If all affected persons and the clarification body consider the mediation a
  success, the issue is resolved without any further consequences.

- Suppose an issue cannot be sufficiently resolved even after continued effort
  for a reasonable amount of time. In that case, the issue can be escalated
  further at the discretion of the clarification body.

- If mediation fails, the clarification body can issue a warning to one or more of
  the affected parties. Such a warning will usually be communicated privately to
  the user. The warning needs to be acknowledged in writing. That is required to
  ensure that the user has read and understood the scope of their transgression.

- If the warning goes unheeded, or in case the occurrence is judged to be especially
  flagrant, the clarification body can suggest further actions to be taken. Possible
  actions can be but are not limited to: temporarily or permanently suspending or
  deleting the user’s account, or in extreme cases, a ban from all Arch Linux projects.

- The suggestion by the clarification body is then taken for consideration by
  the appropriate group of Arch Linux staff (e.g. the group of Forum administrators,
  developers, all staff, etc.). Private mailing lists should be preferred for this
  purpose to respect the privacy of the involved parties.

- If the suggestion put forward finds acceptance by the respective group, it is
  implemented by the clarification body. The decision is communicated in private
  to the affected person(s).

Election
++++++++

The role of Arch Linux Mediator is determined by a vote among eligible members
of the Arch Linux Team.

Nomination
**********

Nomination of candidates for open positions will take place in a two week
period prior to voting. Nominations require support of two members of the Arch
Linux Team, which may include the nominee as well as the current project leader.
If the nominee is neither of the nominators, then they must also agree to be
nominated. All Team members are eligible for nomination.

Voting
******

When more candidates than open positions are nominated, voting will be
conducted using the instant-runoff voting system. Ballots with candidates
ranked in order of preference will be collected and tallied by a team member
who is neither a candidate nor has nominated a candidate. Voting shall take
place over a two week period, with no quorum in effect.

When less candidates than open positions are nominated, the voting system is
changed to combined approval voting, in which candidates receive either an
approval or disapproval vote. If a nominee has more approval votes, than
disapproval votes, then they are approved for one position. If, on the other
hand, a nominee has more disapproval votes, than approval votes, they are not
approved for the position, regardless if they are the sole contender for it.

Team members eligible for voting are:

* Arch Linux Developers
* Arch Linux Trusted Users
* Arch Linux Support Staff

The complete list of individuals in these roles is defined on the Arch Linux
website, fixed at the beginning of the voting period. People who have roles
in multiple of these categories are only eligible to vote once.

Drawbacks
---------

Handling disputes with external persons may create additional load on the
mediators. However, having a total of three available mediators to balance
between should mitigate potential burnout. By covering the interaction between
staff members and external people we ensure that we achieve both: protect our
staff members across all aspects of Arch Linux as well as protect the values of
our community and their participants.

Unresolved Questions
--------------------

None.

