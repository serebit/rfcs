=========================================
Adopt PEP 517 tooling for Python packages
=========================================

- Date proposed: 2022-02-19
- RFC MR: https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/0010

Summary
-------

Replace setup.py with PEP 517 tooling (python-build/python-installer) for all
Python packages where this approach is viable.

Motivation
----------

There are two main points.

1) Direct ``setup.py`` calls are deprecated

The setuptools upstream has deprecated direct ``setup.py`` calls, and plans to
remove support for them in the future. Please see `setuptools #2088`_ and
`setuptools #2080`_ for more context.

2) ``setup.py install`` calls install old-style Python metadata

The metadata installed by ``setup.py install`` is the old-style "egg" format
(.egg-info). Is is essentially implementation-defined by setuptools and has been
replaced by a standardized format (.dist-info).
By moving to the PEP 517 tooling, we will be using the wheel format, which has
the standardized metadata.

Specification
-------------

The change consists of replacing ``setup.py`` invocations with the
``python-build`` and ``python-installer`` tools.

Essentially replacing

::

  python setup.py build
  python setup.py install --root="$pkgdir" --optimize=1

with

::

  python -m build --wheel --no-isolation
  python -m installer --destdir="$pkgdir" dist/*.whl

You can find more information in the `Python packaging guidelines`_.

This would naturally only serve as a general guideline, packages that need an
alternative approach can still do so, but are recommended to stay as close as the
guidelines as reasonably possible.

Drawbacks
---------

This change affects a big number of packages, so it will require some work per
part of the maintainers, but it can be done incrementally as packages are
updated.

Unresolved Questions
--------------------

None, I think.

Alternatives Considered
-----------------------

I don't think there is currently any viable alternative solution for the issues
presented.


.. _setuptools #2088: https://github.com/pypa/setuptools/issues/2088
.. _setuptools #2080: https://github.com/pypa/setuptools/issues/2080
.. _Python packaging guidelines: https://wiki.archlinux.org/title/Python_package_guidelines#Standards_based_(PEP_517)
