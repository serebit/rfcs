==========================
Merge package repositories
==========================

- Date proposed: 2022-09-01
- RFC MR: https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/14

Summary
-------

Merge the pacman repository `[community]` into `[extra]` as well as the
packaging VCS location `community` into `packages`.

The `[core]` repository remains limited to Developers.

New Package Maintainers will be restricted to publish packages solely to
`[testing]` for the first two months of their active packaging actions.


Motivation
----------

In early days, the Trusted User packagers were meant to be a completely
decoupled group from Arch Linux and hence got a dedicated optional pacman
repository and VCS. Over the time, the distinction of packages between
`[community]` and `[extra]` became blurry and at the latest since the official
re-integration and rename of the Trusted Users into the Arch Linux organization
the barrier became moot and essentially just obstructive.

Merging `[community]` and `[extra]` will greatly expand the packager coverage
and reduce the common need to move `[extra]` packages to `[community]` just to
allow Package Maintainers that have no Developer permissions to maintain
ordinary packages.

Nevertheless as `[extra]` will contain a lot of system critical packages like
widely used libraries and desktop environments we intend to shield them from
accidents of new Package Maintainers.

Furthermore the unification of the VCS source locations greatly reduces the
complexity and will dramatically simplify the Git migration. Without the merge
a lot of Git packaging workflow issues and inconveniences arise like cross repo
history moves as well as invalid local clones afterwards. Once we have one
namespace for packaging sources, we would likely be able to propose a final Git
migration implementation shortly.

As a cherry on top, this structure will easily allow us to have Junior Developers.


Specification
-------------

Use a dedicated set of repositories for `[core]` that has access control for
package release and db-move limited to Developers:

- core
- core-testing
- core-staging

Merge the `[community]` repository into `[extra]` including the testing and staging
repositories with access limited to Developers and Package Maintainers:

- extra
- testing
- staging
- multilib
- multilib-testing
- multilib-staging

New Package Maintainers will be restricted to publish packages solely to
`[testing]` for the first two months of their active packaging actions. Their
sponsors are expected to be the primary persons responsible for carefully
reviewing changes before moving them out of `[testing]`.

Unify the VCS location into a single primary Git group namespace. This will allow
us to have all individual package PKGBUILD repositories in a single location with
a convenient and simpler workflow:

- https://gitlab.archlinux.org/archlinux/packaging/packages


Drawbacks
---------

- Trusted Users will be able to commit to PKGBUILDs related to `[core]` packages.
  However this shouldn't be an issue if we still have access control in front
  of the `[core]` package releasing process. The package release is the
  important part that should be shielded and Trusted Users are no external group
  anymore.


Unresolved Questions
--------------------

None


Alternatives Considered
-----------------------

- Arch could do nothing instead, but that will preserve all the problems like
  the artificial difference between `[extra]` and `[community]`, the reduced
  exposure of `[extra]` to all packagers as well as complicate the Git migration
  and heavily inconvenience its resulting packaging workflows.

- If the VCS location merge is done before migrating to Git, we either heavily
  complicate the svn2git conversion or lose history. Therefore it is
  recommended to only go for the VCS location merge while migrating to Git.
